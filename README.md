Project: EveJarvis.com

The application is developed to calculate the cost of producing in-game items in the eve-online.

How to do that?

1) Enter the name of the item you are looking for in the text field.

![alt text](/readmeImg/1.jpg/)

2) Choose your item from the options provided.

![alt text](/readmeImg/2.jpg/)

3) if the item contains additional blueprints, indicate them in the 2nd window (the window number is indicated in the upper right), and also indicate the percentage of material efficiency for the main blueprint and for components

![alt text](/readmeImg/3.jpg/)

4) Next, in window 3, indicate the type of the structure on which the production will be carried out, whether rigs are used in the station, as well as the type of system where the station is located. And don't forget to hit "APPLY" button when done to apply changes to the blueprint.

![alt text](/readmeImg/4.jpg/)

5) And finally, all that remains is to press the "UPDATE PRICE" button to download the current prices from Jita and calculate the final result.

![alt text](/readmeImg/5.jpg/)
