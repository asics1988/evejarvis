const bigNumbersFormatter = (num) => {
  return num
    ?.toString()
    ?.replace(/\s/g, "")
    ?.toString()
    ?.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};

const findBasicMaterials = (blueprint, quantity = 1) => {
  return blueprint?.activities?.manufacturing?.materials?.map((material) => {
    if (material?.ME != null) {
      return findBasicMaterials(material?.blueprint, material?.quantity || 1);
    }
    return {
      ...material,
      quantity: material?.quantity * quantity,
    };
  });
};

const prepareMaterialList = (materialList, typeID) =>
  materialList.reduce((accum, mat) => {
    if (mat?.typeID === typeID) {
      return (accum = {
        name: mat?.name,
        quantity: (accum?.quantity || 0) + mat?.quantity,
        typeID: mat?.typeID,
      });
    }
    return accum;
  }, 0);

const materialInArray = (calculatedMaterialList, typeID) =>
  calculatedMaterialList.find((material) => material?.typeID === typeID);

const prepareMaterials = (blueprint) => {
  const materialList = findBasicMaterials(blueprint)?.flat();

  const calculatedMaterialList = [];

  materialList?.forEach((material) => {
    if (!materialInArray(calculatedMaterialList, material?.typeID)) {
      calculatedMaterialList.push(
        prepareMaterialList(materialList, material?.typeID)
      );
    }
  });

  return calculatedMaterialList;
};

export { bigNumbersFormatter };
export { prepareMaterials };
