import { useEffect, useState } from "react";

import WindowWrapper from "@src/Components/WindowWrapper/WindowWrapper";
import MaterialRow from "@src/Components/Controls/MaterialRow/MaterialRow";

export default ({ bluePrintData, updateBlueprint }) => {
  const [blueprint, setBlueprint] = useState(null);

  const updateBP = (newBPUpdate) => {
    const BP = { ...blueprint };
    BP.activities.manufacturing.materials.map((material, index) => {
      material.name === newBPUpdate.name
        ? (BP.activities.manufacturing.materials[index] = newBPUpdate)
        : null;
    });
    setBlueprint(BP);
    updateBlueprint(BP);
  };

  useEffect(() => {
    setBlueprint(bluePrintData);
  }, [bluePrintData]);

  return (
    <WindowWrapper
      style={{
        position: "relative",
        paddingTop: 40,
        margin: "0 auto",
      }}
      classN={"test"}
    >
      <div
        style={{
          top: 5,
          right: 15,
          fontWeight: 700,
          fontSize: 25,
          position: "absolute",
          color: "#30b2e6",
        }}
      >
        2
      </div>
      {blueprint
        ? blueprint.activities.manufacturing.materials.map((material, key) => {
            return (
              <div key={key}>
                <MaterialRow data={material} updateBP={updateBP} />
              </div>
            );
          })
        : null}
    </WindowWrapper>
  );
};
