import styles from "./styles.module.scss";

export default ({ children, style, classN }) => {
  return (
    <div className={`${styles.wrapper} ${styles[classN]}`} {...{ style }}>
      {children}
    </div>
  );
};
