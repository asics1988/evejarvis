import { useState, useEffect } from "react";

import request from "@src/api/request";
import Button from "@src/Components/Controls/Button/Button";
import InputField from "@src/Components/Controls/InputField/InputField";
import WindowWrapper from "@src/Components/WindowWrapper/WindowWrapper";
import Switch from "@src/Components/Controls/Switch/Switch";
import {
  minMarketsOrdersPraser,
  maxMarketsOrdersPraser, // refactor this
} from "@src/apiDataParsers/marketOrders";

import { bigNumbersFormatter } from "@src/supportingFunctional/supportingFunctional";

import styles from "./styles.module.scss";

export default ({ materialsObj }) => {
  const [mineralsData, setMineralsData] = useState(null);
  const [pricedMineralsData, setPricedMineralsData] = useState(null);
  const [totalMineralsPrice, setTotalMineralsPrice] = useState(null);
  const [mineralsIDs, setMineralsIDs] = useState(null);
  const [orderState, setOrderState] = useState("sell");

  useEffect(() => {
    if (materialsObj) {
      setMineralsIDs(materialsObj);
    }
  }, [materialsObj]);

  const totalPriceCalculator = (pricedData) => {
    const result = pricedData.reduce((accum, current) => {
      return accum + current.price * current.quantity;
    }, 0);

    return Math.round(result);
  };

  useEffect(() => {
    pricedMineralsData
      ? setTotalMineralsPrice(totalPriceCalculator(pricedMineralsData))
      : null;
  }, [pricedMineralsData]);

  const mineralRequest = (type_id, orderType) =>
    request({
      method: "marketsOrders",
      region_id: "10000002",
      params: {
        order_type: orderType, //"sell" or "buy"
        type_id,
      },
    });

  const requestOrders = () => {
    if (mineralsIDs) {
      Promise.all(
        mineralsIDs.map((material) => {
          return mineralRequest(material.typeID, orderState);
        })
      )
        .then(setMineralsData)
        .catch(console.log);
    }
  };

  useEffect(() => {
    if (mineralsData) {
      orderState === "sell"
        ? setPricedMineralsData(
            minMarketsOrdersPraser(mineralsData, mineralsIDs)
          )
        : setPricedMineralsData(
            maxMarketsOrdersPraser(mineralsData, mineralsIDs)
          );
    }
  }, [mineralsData]);

  return (
    <WindowWrapper classN={"test"} style={{ position: "relative" }}>
      <div
        style={{
          top: 5,
          right: 15,
          fontWeight: 700,
          fontSize: 25,
          position: "absolute",
          color: "#30b2e6",
        }}
      >
        4
      </div>
      {mineralsIDs?.map?.((mineral, index) => (
        <div className={styles.mineralsDiv} key={index}>
          <div className={styles.mineralsTitle}>{mineral.name}</div>
          <InputField
            placeholder={mineral.quantity}
            isMoney={true}
            style={{ padding: "0 20px" }}
          />
          <div className={styles.price}>
            x {pricedMineralsData ? pricedMineralsData[index].price : 0} isk
          </div>
        </div>
      ))}

      <div className={styles.bottomWrapper}>
        <div className={styles.totalCost}>
          {`Total cost: ${
            totalMineralsPrice ? bigNumbersFormatter(totalMineralsPrice) : 0
          } isk`}
        </div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <div style={{ textAlign: "center" }}>
            <Switch
              checked
              onChange={(state) => setOrderState(state ? "buy" : "sell")}
            />
          </div>
          <Button title="Update prices" onClick={() => requestOrders()} />
        </div>
      </div>
    </WindowWrapper>
  );
};
