import styles from "./styles.module.scss";

export default ({ children, style }) => {
  return (
    <div className={styles.wrapper} {...{ style }}>
      {children}
    </div>
  );
};
