import { useEffect, useState } from "react";

import WindowWrapper from "@src/Components/WindowWrapper/WindowWrapper";
import SelectList from "@src/Components/Controls/SelectList/SelectList";
import Button from "@src/Components/Controls/Button/Button";

import styles from "./styles.module.scss";

export default ({ bluePrintData, updateBlueprint }) => {
  const [isDisabled, setIsDisabled] = useState(false);
  const skillsLvl = [0, 1, 2, 3, 4, 5];
  const implants = ["none", "BX-801", "BX-802", "BX-804"];
  const structureValue = ["Other", "Azbel", "Raitaru", "Sotiyo"];
  const [bluePrint, setBluePrint] = useState(null);
  const [rigValue, setRigValue] = useState(false);
  const [structureModModifier, setStructureModModifier] = useState(0);
  const [craftInModifier, setCraftInModifier] = useState(1);
  const [industryModifier, setIndustryModifier] = useState(null);
  const [advancedIndustryModifier, setAdvancedIndustryModifier] = useState(
    null
  );
  const [implantModifier, setImplantModifier] = useState(null);

  useEffect(() => {
    setBluePrint(bluePrintData);
  }, [bluePrintData]);

  useEffect(() => {
    if (bluePrint) {
      updateBlueprint(bluePrint);
    }
  }, [bluePrint]);

  const calculateTE = (params) => {
    const bp = { ...bluePrint };
    const bpTE = bluePrint?.activities?.manufacturing?.TE;

    bp.activities.manufacturing.time =
      (bp.activities.manufacturing.time *
        (100 -
          (params.industryModifier +
            params.advancedIndustryModifier +
            params.implantModifier +
            bpTE))) /
      100;
    bp.activities.manufacturing.materials.map((material) => {
      if (material.blueprint) {
        material.blueprint.activities.manufacturing.time =
          (material.blueprint?.activities?.manufacturing?.time *
            (100 -
              (params.industryModifier +
                params.advancedIndustryModifier +
                params.implantModifier +
                (material.TE ? material.TE : 0)))) /
          100;
      }
    });
    setBluePrint(bp);
  };

  const calculateME = (params) => {
    const bp = { ...bluePrint };
    const bpME = bluePrint?.activities?.manufacturing?.ME;
    bp?.activities?.manufacturing?.materials?.map((material) => {
      const quantityMultiplier =
        (((((1 * (100 - bpME)) / 100) * (100 - params.structureModModifier)) /
          100) *
          (100 - params.craftInModifier)) /
        100;
      material.quantity = Math.ceil(material.quantity * quantityMultiplier);

      if (material.blueprint) {
        material.blueprint.activities?.manufacturing?.materials?.map((item) => {
          const quantityMultiplier =
            (((((1 * (100 - (material.ME ? material.ME : 0))) / 100) *
              (100 - params.structureModModifier)) /
              100) *
              (100 - params.craftInModifier)) /
            100;
          item.quantity = Math.round(item.quantity * quantityMultiplier);
        });
      }
    });
    setBluePrint(bp);
  };

  return (
    <WindowWrapper
      style={{ position: "relative", maxWidth: 500, margin: "0 auto" }}
    >
      <div
        style={{
          top: 5,
          right: 15,
          fontWeight: 700,
          fontSize: 25,
          position: "absolute",
          color: "#30b2e6",
        }}
      >
        3
      </div>
      <div style={{ fontWeight: 700, fontSize: 20, paddingBottom: 10 }}>
        Efficiency:
      </div>
      <div style={{ display: "flex", paddingBottom: 5 }}>
        <div className={styles.chooseBlueprintWrapper}>
          <div className={styles.BPTitle}>Structure: </div>
          <div style={{ marginRight: 15 }}>
            <SelectList
              efficiencyValueOptions={structureValue}
              isValue={(value) => {
                switch (value) {
                  case "Other":
                    setStructureModModifier(0);
                    break;
                  case "Azbel":
                    setStructureModModifier(1);
                    break;
                  case "Raitaru":
                    setStructureModModifier(1);
                    break;
                  case "Sotiyo":
                    setStructureModModifier(1);
                    break;
                }
              }}
            />
          </div>
        </div>
        <div>
          <label style={{ outline: "none", cursor: "pointer" }}>
            Efficiency Rig?
            <input
              type="checkbox"
              onChange={(event) => {
                setRigValue(event.target.checked);
              }}
              style={{ cursor: "pointer" }}
            />
          </label>
        </div>
      </div>
      {rigValue ? (
        <div style={{ display: "flex" }}>
          <div style={{ paddingRight: 10 }}>Craft in:</div>

          <div style={{ paddingRight: 10 }}>
            <label style={{ cursor: "pointer" }}>
              HI-SEC
              <input
                type="radio"
                name="craftIn"
                style={{ cursor: "pointer" }}
                onChange={() => {
                  setCraftInModifier(1 * 2);
                }}
              />
            </label>
          </div>
          <div style={{ paddingRight: 10 }}>
            <label style={{ cursor: "pointer" }}>
              LOW-SEC
              <input
                type="radio"
                name="craftIn"
                style={{ cursor: "pointer" }}
                onChange={() => {
                  setCraftInModifier(1.9 * 2);
                }}
              />
            </label>
          </div>
          <div>
            <label style={{ cursor: "pointer" }}>
              Null/Wormhole
              <input
                type="radio"
                name="craftIn"
                style={{ cursor: "pointer" }}
                onChange={() => {
                  setCraftInModifier(2.1 * 2);
                }}
              />
            </label>
          </div>
        </div>
      ) : null}

      <div>
        <div
          style={{
            fontWeight: 700,
            fontSize: 20,
            paddingBottom: 10,
            paddingTop: 10,
          }}
        >
          Skills and Implants:
        </div>
        <div style={{ display: "flex", paddingBottom: 10 }}>
          <div className={styles.chooseBlueprintWrapper}>
            <div className={styles.BPTitle}>Industry: </div>
            <div style={{ marginRight: 15 }}>
              <SelectList
                efficiencyValueOptions={skillsLvl}
                isValue={(value) => {
                  setIndustryModifier(+value);
                }}
              />
            </div>
          </div>
          <div className={styles.chooseBlueprintWrapper}>
            <div className={styles.BPTitle}>Advanced Industry: </div>
            <div style={{ marginRight: 15 }}>
              <SelectList
                efficiencyValueOptions={skillsLvl}
                isValue={(value) => setAdvancedIndustryModifier(+value)}
              />
            </div>
          </div>
        </div>
        <div
          className={styles.chooseBlueprintWrapper}
          style={{ paddingBottom: 20 }}
        >
          <div className={styles.BPTitle}>Implant: </div>
          <div>
            <SelectList
              efficiencyValueOptions={implants}
              isValue={(value) => {
                switch (value) {
                  case "none":
                    setImplantModifier(0);
                    break;
                  case "BX-801":
                    setImplantModifier(1);
                    break;
                  case "BX-802":
                    setImplantModifier(2);
                    break;
                  case "BX-804":
                    setImplantModifier(4);
                    break;
                }
              }}
            />
          </div>
        </div>
      </div>
      <div style={{ paddingLeft: "85%" }}>
        <Button
          style={
            isDisabled && bluePrint
              ? { pointerEvents: "none", background: "grey" }
              : {
                  pointerEvents: "auto",
                  background: `linear-gradient(rgb(48, 178, 230) 0%, rgb(43, 158, 204) 100%),
              rgb(48, 178, 230)`,
                }
          }
          title={"Apply"}
          onClick={() => {
            if (bluePrint) {
              setIsDisabled(true);
              calculateME({
                structureModModifier: structureModModifier,
                craftInModifier: craftInModifier,
              });
              calculateTE({
                industryModifier: industryModifier,
                advancedIndustryModifier: advancedIndustryModifier,
                implantModifier: implantModifier,
              });
            }
          }}
        />
      </div>
    </WindowWrapper>
  );
};
