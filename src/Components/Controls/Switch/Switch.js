import React, { useEffect, useState } from "react";

import styles from "./styles.module.scss";

export default ({ style, name, active = false, onChange = () => null }) => {
  const [value, setValue] = useState(active);

  useEffect(() => {
    onChange(value);
  }, [value]);

  return (
    <div className={styles.center} {...{ style }}>
      <div style={{ position: "absolute", fontWeight: 600 }}>Orders</div>
      <input
        type="checkbox"
        name={name}
        className={styles.input}
        checked={value}
        onChange={() => setValue((oldState) => !oldState)}
      />
    </div>
  );
};
