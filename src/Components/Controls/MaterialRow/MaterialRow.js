import { useEffect, useState } from "react";

import { bigNumbersFormatter } from "@src/supportingFunctional/supportingFunctional";
import SelectList from "@src/Components/Controls/SelectList/SelectList";

import blueprintIcon from "@src/assets/images/b6d4ce0f1acb0ae3196a9f8a0829bdd7.png";

import styles from "./styles.module.scss";

export default ({ data, updateBP }) => {
  const [material, setMaterial] = useState(null);
  const [additionalBP, setAdditionalBP] = useState(null);
  const materialEfficiencyValueOptions = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const timeEfficiencyValueOptions = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
  const [MEValue, setMEValue] = useState(null); // Material Efficiency
  const [TEValue, setTEValue] = useState(null); // Time Efficiency

  useEffect(() => {
    if (material) {
      updateBP(material);
    }
  }, [material]);

  useEffect(() => {
    setMaterial(data);
  }, [data]);

  useEffect(() => {
    if (MEValue != null || TEValue != null) {
      const state = { ...material };
      state.ME = MEValue ? +MEValue : 0;
      state.TE = TEValue ? +TEValue : 0;
      setMaterial({ ...state });
    }
  }, [MEValue, TEValue]);

  return (
    <div>
      <div className={styles.rowWrapper}>
        {additionalBP ? <div className={styles.addRowModifier}></div> : null}
        <div className={styles.imgTitleWrapper}>
          <div className={styles.materialIcon}>
            {material?.typeID ? (
              <img
                src={`https://images.evetech.net/types/${material?.typeID}/icon`}
              />
            ) : null}
          </div>
          <div className={styles.materialName}>{material?.name}</div>
          {material?.blueprint ? (
            <div
              title={"Add blueprint for this component"}
              style={{ width: 25, height: 25, cursor: "pointer" }}
              onClick={() => {
                setAdditionalBP(material?.blueprint);
                setMaterial((oldState) => {
                  const state = { ...oldState };
                  state.ME = 0;
                  state.TE = 0;
                  return state;
                });
              }}
            >
              <img
                src={blueprintIcon}
                style={{ width: "100%", height: "100%" }}
              />
            </div>
          ) : null}
        </div>
        <div className={styles.materialQuantity}>
          quantity:{" "}
          {material?.quantity ? bigNumbersFormatter(material.quantity) : null}
        </div>
      </div>
      {additionalBP ? (
        <div className={styles.addWrapper}>
          <div className={styles.additionalRow}></div>
          <div className={`${styles.rowWrapper} ${styles.additionalRow}`}>
            <div style={{ width: 25, height: 25, display: "flex" }}>
              <img
                src={blueprintIcon}
                style={{ width: "100%", height: "100%" }}
              />
            </div>

            <div style={{ display: "flex" }}>
              <div className={styles.efficiencyWrapper}>
                <div className={styles.BPTitle}>ME:</div>
                <div style={{ marginRight: 15 }}>
                  <SelectList
                    isValue={(MEValue) => setMEValue(MEValue)}
                    efficiencyValueOptions={materialEfficiencyValueOptions}
                  />
                </div>
                <div className={styles.BPTitle}>TE: </div>
                <div style={{ marginRight: 15 }}>
                  <SelectList
                    isValue={(TEValue) => setTEValue(TEValue)}
                    efficiencyValueOptions={timeEfficiencyValueOptions}
                  />
                </div>
              </div>
              <div
                style={{ fontWeight: 700, color: "red", cursor: "pointer" }}
                onClick={() => {
                  setTEValue(null);
                  setMEValue(null);
                  setAdditionalBP(null);
                  setMaterial((oldState) => {
                    const state = { ...oldState };
                    delete state.ME;
                    delete state.TE;
                    return state;
                  });
                }}
              >
                X
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
};
