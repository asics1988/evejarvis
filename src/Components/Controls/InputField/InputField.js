import { useState, useEffect } from "react";

import styles from "./styles.module.scss";

import { bigNumbersFormatter } from "@src/supportingFunctional/supportingFunctional";

export default ({
  placeholder,
  style,
  onClick,
  isMoney,
  isValue,
  changeValue,
}) => {
  const [value, setValue] = useState("");
  const [replacedValue, setReplacesValue] = useState("");

  useEffect(() => {
    changeValue ? setReplacesValue(changeValue) : setReplacesValue("");
  }, [changeValue]);

  useEffect(() => {
    if (isMoney) {
      setReplacesValue(bigNumbersFormatter(value));
    } else {
      setReplacesValue(value);
    }
    if (isValue) {
      isValue(value);
    }
  }, [value]);

  return (
    <div className={styles.wrapper} {...{ style, onClick }}>
      <input
        placeholder={placeholder}
        style={{ width: "100%", height: "100%", outline: "none" }}
        onChange={(event) => setValue(event.target.value)}
        value={replacedValue}
      />
    </div>
  );
};
