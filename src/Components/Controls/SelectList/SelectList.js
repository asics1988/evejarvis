import { useState, useEffect } from "react";

import styles from "./styles.module.scss";

export default ({
  efficiencyValueOptions,
  isValue,
  lockAfterValue = false,
}) => {
  const [value, setValue] = useState("");
  const [replacedValue, setReplacesValue] = useState("");

  useEffect(() => {
    !lockAfterValue ? setReplacesValue(0) : null;
  }, [lockAfterValue]);

  useEffect(() => {
    setReplacesValue(value);

    if (isValue) {
      isValue(value);
    }
  }, [value]);
  return (
    <div
      className={styles.wrapper}
      title={value && lockAfterValue ? "Locked" : null}
    >
      <select
        id="myList"
        style={
          ({ outline: "none" },
          value && lockAfterValue
            ? {
                pointerEvents: "none",
                backgroundColor: "#1daee7",
                color: "#fff",
              }
            : null)
        }
        onChange={(event) => setValue(event.target.value)}
        value={replacedValue}
      >
        {efficiencyValueOptions.map((item, key) => (
          <option value={item} key={key}>
            {item}
          </option>
        ))}
      </select>
    </div>
  );
};
