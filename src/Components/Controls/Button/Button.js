import react from "react";

import styles from "./styles.module.scss";

export default ({ title = "Текст кнопки", style, onClick }) => {
  return (
    <div className={styles.button} {...{ style, onClick }}>
      {title}
    </div>
  );
};
