import { useEffect, useState } from "react";

import request from "@src/api/request";
import WindowWrapper from "@src/Components/WindowWrapper/WindowWrapper";
import SearchResultField from "@src/Components/SearchResultField/SearchResultField";
import InputField from "@src/Components/Controls/InputField/InputField";
import SelectList from "@src/Components/Controls/SelectList/SelectList";
import Button from "@src/Components/Controls/Button/Button";

import loader from "@src/assets/images/loader.svg";

import styles from "./styles.module.scss";

export default ({ setBluePrint }) => {
  const materialEfficiencyValueOptions = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const timeEfficiencyValueOptions = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
  const [BPValue, setBPValue] = useState("");
  const [MEValue, setMEValue] = useState(0); // Material Efficiency
  const [TEValue, setTEValue] = useState(0); // Time Efficiency
  const [searchedResults, setSearchedResults] = useState(null);
  const [choosedBPID, setChoosedBPID] = useState(null);
  const [choosedBlueprint, setChoosedBlueprint] = useState(null);
  const [load, setLoad] = useState(false);
  const [requestDone, setRequestDone] = useState(true);
  const [prevRequest, setPrevRequest] = useState(null);
  const [lockBPValue, setLockBPValue] = useState(false);

  const blueprintIDRequest = (name) =>
    request({
      method: "getBlueprintID",
      params: {
        name,
      },
    });

  const blueprintRequest = (blueprintID) =>
    request({
      method: "getBlueprint",
      params: {
        blueprintID,
      },
    });

  const appendMETEFieldsToBP = (blueprint, ME = 0, TE = 0) => {
    const result = { ...blueprint };
    result.activities.manufacturing.ME = +ME;
    result.activities.manufacturing.TE = +TE;
    return result;
  };

  useEffect(() => {
    if (choosedBlueprint != null) {
      const updatedBlueprint = appendMETEFieldsToBP(
        choosedBlueprint,
        MEValue,
        TEValue
      );
      setChoosedBlueprint(updatedBlueprint);
    }
  }, [MEValue, TEValue]);

  useEffect(() => {
    setBluePrint(choosedBlueprint);
    setLoad(false);
    if (choosedBlueprint && (MEValue > 0 || TEValue > 0)) {
      setLockBPValue(true);
    } else {
      setLockBPValue(false);
    }
  }, [choosedBlueprint]);

  useEffect(() => {
    choosedBPID
      ? blueprintRequest(choosedBPID[1].toString()).then((res) => {
          setChoosedBlueprint(res[1]);
          appendMETEFieldsToBP(res[1], MEValue, TEValue);
        })
      : null;
  }, [choosedBPID]);

  const requestWithConditions = () => {
    if (BPValue.length >= 3 && requestDone) {
      setRequestDone(false);
      setPrevRequest(BPValue);
      blueprintIDRequest(BPValue).then((res) => {
        setSearchedResults(res);
        setRequestDone(true);
      });
    }
    BPValue.length < 3 ? setSearchedResults(null) : null;
  };

  useEffect(() => {
    requestWithConditions();
  }, [BPValue]);

  useEffect(() => {
    if (prevRequest !== BPValue) {
      requestWithConditions();
    }
  }, [requestDone]);

  function refreshPage() {
    window.location.reload();
  }

  return (
    <WindowWrapper
      style={{
        maxWidth: 500,
        margin: "0 auto",
        position: "relative",
      }}
    >
      <div
        style={{
          top: 5,
          right: 15,
          fontWeight: 700,
          fontSize: 25,
          position: "absolute",
          color: "#30b2e6",
        }}
      >
        1
      </div>

      <div style={{ paddingBottom: 10 }}>
        <div className={styles.chooseBlueprintWrapper}>
          <div className={styles.BPTitle}>Blueprint: </div>
          <div style={{ paddingRight: 15 }}>
            <InputField
              placeholder={"Example: Heron"}
              isValue={(BPValue) => setBPValue(BPValue)}
              changeValue={
                choosedBPID && choosedBPID != "" ? choosedBPID[0] : ""
              }
            />
          </div>

          <div className={styles.METEWrapper}>
            <div className={styles.BPTitle}>ME:</div>
            <div style={{ marginRight: 15 }}>
              <SelectList
                isValue={(MEValue) => {
                  setMEValue(MEValue);
                }}
                efficiencyValueOptions={materialEfficiencyValueOptions}
                lockAfterValue={lockBPValue}
              />
            </div>
            <div className={styles.BPTitle}>TE: </div>
            <div style={{ marginRight: 15 }}>
              <SelectList
                isValue={(TEValue) => setTEValue(TEValue)}
                efficiencyValueOptions={timeEfficiencyValueOptions}
                lockAfterValue={lockBPValue}
              />
            </div>
          </div>
        </div>
      </div>
      <div style={{ overflowY: "auto", maxHeight: 400 }}>
        {searchedResults && searchedResults.length > 0 ? (
          <SearchResultField style={{ marginBottom: 11 }}>
            {searchedResults?.map((bp, key) => {
              return (
                <div
                  key={key}
                  onClick={() => {
                    setChoosedBPID(bp);
                    setSearchedResults(null);
                    setLoad(true);
                  }}
                  className={styles.searchedBP}
                >
                  {bp[0]}
                </div>
              );
            })}
          </SearchResultField>
        ) : null}
      </div>
      <div
        style={{
          height: 40,
          textAlign: "center",
          display: load ? "block" : "none",
        }}
      >
        <img src={loader} width="100%" height="100%" />
      </div>

      {choosedBlueprint ? (
        <div style={{ textAlign: "end" }}>
          <Button title="Reset" onClick={() => refreshPage()} />
        </div>
      ) : null}
    </WindowWrapper>
  );
};
