import { useEffect, useState } from "react";

import { prepareMaterials } from "@src/supportingFunctional/supportingFunctional";

import MineralsTotalCount from "./Components/MineralsTotalCount/MineralsTotalCount";
import BlueprintDetector from "@src/Components/BlueprintDetector/BlueprintDetector";
import EfficiencyCalculator from "@src/Components/EfficiencyCalculator/EfficiencyCalculator";
import MaterialsBoard from "@src/Components/MaterialsBoard/MaterialsBoard";
import Button from "@src/Components/Controls/Button/Button";

import logo from "@src/assets/images/logo_eve_jarvis.svg";

import styles from "./App.module.scss";

function App() {
  const [bluePrintData, setBluePrintData] = useState(null);
  const setBluePrint = (data) => setBluePrintData(data);
  const updateBlueprint = (data) => setBluePrintData(data);
  const [materialsObj, setMaterialsObj] = useState(null);

  useEffect(() => {
    if (bluePrintData) {
      setMaterialsObj(prepareMaterials(bluePrintData));
    }
  }, [bluePrintData]);

  return (
    <div className={styles.app}>
      <div className={styles.left}>
        <div className={styles.guide}>
          <a href={"https://gitlab.com/asics1988/evejarvis"} target="blank">
            <Button
              title={"Guide"}
              style={{
                background: "rgb(250, 215, 77)",
                color: "#000",
                fontWeight: 600,
              }}
            />
          </a>
        </div>
        <div className={styles.blueprintDetector}>
          <BlueprintDetector {...{ setBluePrint }} />
        </div>
        {bluePrintData ? (
          <div className={styles.efficiencyCalculator}>
            <EfficiencyCalculator
              {...{ bluePrintData }}
              {...{ updateBlueprint }}
            />
          </div>
        ) : null}
      </div>
      <div className={styles.center}>
        <div>
          {bluePrintData ? (
            <MaterialsBoard {...{ bluePrintData }} {...{ updateBlueprint }} />
          ) : null}
        </div>
      </div>
      <div className={styles.right}>
        <div>
          {bluePrintData ? <MineralsTotalCount {...{ materialsObj }} /> : null}
        </div>
      </div>

      <div className={styles.logo}>
        <img src={logo} style={{ width: "100%", height: "100%" }} />
      </div>
    </div>
  );
}

export default App;

// 30000142 - jita id
// 10000002 - The Forge - jita region
