// REFACTOR THIS!!!
const minMarketsOrdersPraser = (mineralsData, mineralsIDs) => {
  let minPrice;

  const findMinPrice = (mineralObject, mineralsData) => {
    minPrice = 9999999999;
    const updatedMineralObject = { ...mineralObject };

    Object.values({ ...mineralsData }).forEach((marketObject) => {
      marketObject?.price < minPrice ? (minPrice = marketObject?.price) : null;
    });

    updatedMineralObject.price = minPrice;

    return updatedMineralObject;
  };

  return mineralsIDs.map((mineralObject, index) => {
    return findMinPrice(mineralObject, mineralsData[index]);
  });
};

// REFACTOR THIS!!!
const maxMarketsOrdersPraser = (mineralsData, mineralsIDs) => {
  let maxPrice;

  const findMaxPrice = (mineralObject, mineralsData) => {
    maxPrice = 0;
    const updatedMineralObject = { ...mineralObject };

    Object.values({ ...mineralsData }).forEach((marketObject) => {
      marketObject?.price > maxPrice ? (maxPrice = marketObject?.price) : null;
    });

    updatedMineralObject.price = maxPrice;

    return updatedMineralObject;
  };

  return mineralsIDs.map((mineralObject, index) => {
    return findMaxPrice(mineralObject, mineralsData[index]);
  });
};

export { minMarketsOrdersPraser, maxMarketsOrdersPraser };
