import axios from "axios";

import endPoint from "./endPoints";

const getRequestParams = (data) => {
  const props = endPoint[data?.method];
  const method = props?.requestType;
  const url = props?.url;
  const params = { ...props?.params, ...data?.params };
  const requestString = props?.requestParams;
  const requestParams = requestString
    ?.map?.((item) => {
      if (data[item]) {
        return data[item];
      }
      return item;
    })
    ?.join?.("/");

  return { method, params, requestParams, url };
};

export default (data) => {
  const { method, params, requestParams, url } = getRequestParams(data);
  return axios({
    method,
    url: url + requestParams,
    params,
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      return response?.data;
    })
    .catch((error) => Promise.reject(error));
};
