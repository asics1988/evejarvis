export default {
  marketsOrders: {
    requestType: "GET",
    requestParams: ["markets", "region_id", "orders"],
    url: "https://esi.evetech.net/latest/",
    params: {
      order_type: null,
      region_id: null,
      type_id: null,
      datasource: "tranquility",
    },
  },
  getBlueprintID: {
    requestType: "GET",
    url: "http://evejarvis.com:30321/",
    requestParams: [],
    params: null,
  },

  getBlueprint: {
    requestType: "GET",
    url: "http://evejarvis.com:30321/",
    requestParams: [],
    params: null,
  },
};

// url: "http://evejarvis.com:30321/",
// url: "http://localhost:30321/",
